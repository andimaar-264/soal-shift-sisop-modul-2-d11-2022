# soal-shift-sisop-modul-2-D11-2022


## Members
1. Benedictus Bimo C W - 5025201097
2. Hasna Lathifah Purbaningdyah - 5025201108
3. Muhammad Andi Akbar Ramadhan - 5025201264

## Soal 1

Poin a :
Mendownload File
```
void downloadFile(){
	int status;
	if(fork()==0)
    {
		char temp[50];
		strcpy(temp, dir);
		strcat(temp, "/char.zip");
		char *argv[] = {"wget", "-q", "drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", temp};
		execv("/bin/wget", argv);
	}
	else
    {
		while(wait(&status) > 0);
		char temp[50];
		strcpy(temp, dir);
		strcat(temp, "/weap.zip");
		char *argv2[] = {"wget", "-q", "drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", temp};
        	execv("/bin/wget", argv2);
	}
}
```

Meng-extract file
```
void extractFile(){
	int status;
	if(fork()==0)
    {
		char temp[100];
		strcpy(temp, dir);
		strcat(temp, "/home/dede/modul2");
		
		char *argv[] = {"unzip", "char.zip", "-d", temp, NULL};
		execv("/bin/unzip", argv);
	}
	else
    {
		while(wait(&status) > 0);
		char temp[100];
		strcpy(temp, dir);
		strcat(temp, "/home/dede/modul2");
		
		char *argv2[] = {"unzip", "weap.zip", "-d", temp, NULL};
		execv("/bin/unzip", argv2);
	}
}
```

Pembuatan folder
```
void createDir(){
	pid_t child_id;
	child_id = fork();
	int status;
	if (child_id < 0) {
		exit(EXIT_FAILURE);
  	}
        if(child_id == 0){
		char temp[50];
		strcpy(temp, dir);
		strcat(temp, "/home/dede/modul2");
		char *argv[] = {"mkdir", "-p", temp, NULL};
		execv("/bin/mkdir", argv);
	}
	else
    {
		while(wait(&status) > 0);
		pid_t child_id2;
		//status = NULL;
		child_id2 = fork();
		if (child_id2 < 0) {
		exit(EXIT_FAILURE);
		}
		if(child_id2 == 0){
			downloadFile();
		}
		else{
			while(wait(&status) > 0);
			extractFile();
		}
	}	
}
```

Poin b, c, d :

Gacha
```
void charToTxt(int RNGchar){
	int status;
	char charGet[50];
	strcpy(charGet, "");
	tostring(numgacha);
	strcat(charGet, out);
	strcat(charGet, "_characters_");
	strcat(charGet, rarityChar[RNGchar]);
	strcat(charGet, "_");
	strcat(charGet, nameChar[RNGchar]);
	strcat(charGet, "_");
	tostring(primo);
	strcat(charGet, out);
	strcat(charGet, "\n");
		
        FILE *out=fopen(txtPath,"a+");
	fprintf(out, "%s\n", charGet);
        fclose(out);
}
void weapToTxt(int RNGweap){
	int status;
	char weapGet[50];
	strcpy(weapGet, "");
	tostring(numgacha);
	strcat(weapGet, out);
	strcat(weapGet, "_weapons_");
	strcat(weapGet, rarityWeap[RNGweap]);
	strcat(weapGet, "_");
	strcat(weapGet, nameWeap[RNGweap]);
	strcat(weapGet, "_");
	tostring(primo);
	strcat(weapGet, out);
	strcat(weapGet, "\n");
	
	FILE *out=fopen(txtPath,"a+");
	fprintf(out, "%s\n", weapGet);
	fclose(out);
}
```

Poin e :
Zip the files
```
void zipFile(){
	int status;
	if(fork()==0){
		char *argv[] = {"zip","-r","not_safe_for_wibu.zip","-P", "satuduatiga", "/home/dede/gacha_gacha", NULL};
		execv("/bin/zip", argv);
	}
	else while(wait(&status) > 0); 
}
void deleteFile(){
	int status;
	if(fork()==0){
		char *argv[] = {"rm", "-r", "/home/dede/gacha_gacha", NULL};
		execv("/bin/rm", argv);
	}
	else while(wait(&status) > 0);
}
```


## Soal 2
**Penyelesaian Soal**

**Poin a :** 

Mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Zip yang terextract diharuskan tidak termasuk folder-folder yang tidak dibutuhkan.
 ```
void unzip(char *zipdir, char *locdir)
{
  char *argv[] = {"unzip", zipdir, "*.png", "-d", locdir, NULL};
  execv("/bin/unzip", argv);
}
```
Fungsi ini mengextract zip ke directory yang dituju dengan hanya memasukkan file berformat `.png` saja.


**Poin b :**

Membuat folder drama berdasarkan genre.

```
for (movie = strtok_r(eachMovie, ";", &temp2); movie != NULL; movie = strtok_r(NULL, ";", &temp2))
{
    if (i == 0)
        strcpy(title, movie);
    else if (i == 1)
        strcpy(year, movie);
    else
        strcpy(genre, movie);
    i++;
}
```
Sebelum membuat folder, nama dari file setiap drama akan dipisah berdasarkan `;` untuk mendapatkan judul, tahun, dan genrenya. Dari hasil tersebut, nilai genre akan digunakan untuk membuat folder baru.

```
void mkDir(char *locdir)
{
  char *argv[] = {"mkdir", "-p", locdir, NULL};
  execv("/bin/mkdir", argv);
}
```
Fungsi yang digunakan untuk membuat folder baru. Nilai dari genre digabungkan dengan path dengan menggunakan `strcat`.


**Poin c dan d :**

Memindahkan file ke dalam folder sesuai dengan genrenya masing-masing. Sebelum memindahkan file, untuk file yang mengandung 2 drama, perlu dilakukan pemisahan dan duplikat dari file tersebut.
```
void copyDir(char *locdir, char *locdir1)
{
  char *argv[] = {"cp", locdir, locdir1, NULL};
  execv("/bin/cp", argv);
}
```
Fungsi ini digunakan untuk menduplikat file. Dengan adanya fungsi ini, maka 2 drama yang terdapat dalam 1 file dapat dipindahkan kedalam 2 folder genre sesuai dengan genrenya masing-masing.
```
void moveDir(char *locdir, char *locdir1)
{
  char *argv[] = {"mv", locdir, locdir1, NULL};
  execv("/bin/mv", argv);
}
```
Fungsi ini digunakan untuk memindahkan file dengan tujuan sesuai folder genre masing-masing file.
Bagian pertama ($1) dalam line menandakan IP address.

Melakukan pengecekan ke seluruh kalimat/line dalam file log tersebut kecuali line pertama.

Menyimpan jumlah banyaknya IP yang melakukan request menggunakan array ipAdd[$1], kemudian menghitung total request masing-masing IP menggunakan for loop dan mencari jumlah request yang paling banyak

Menuliskan alamat IP yang melakukan request paling banyak (IPmax) beserta jumlah request nya (max) ke dalam file result.txt


**Poin e :**

Membuat folder `.txt` yang memuat nama kategori, judul drama, dan tahun rilis drama tersebut.
```
void writeTxt(char *locdir, char *movieTitle, char *genre, char *year)
{
  FILE *fp;
  if (access(locdir, F_OK) == -1)
  {
    fp = fopen(locdir, "w");
    fprintf(fp, "kategori : %s\n", genre);
    fclose(fp);
  }

  fp = fopen(locdir, "a");
  fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", movieTitle, year);
  fclose(fp);
}
```
Fungsi ini digunakan untuk menulis isi dari file `.txt`. Dalam penulisannya, terdapat kondisi apakah file `.txt` tersebut sudah ada/belum. Jika file belum ada, maka program akan melakukan write nama kategori yang kemudian akan diikuti oleh append dari judul drama dan tahun rilis drama. Namun jika file sudah ada, maka program tidak akan melakukan write nama kategori dan langsung melakukan append judul drama dan tahun rilis drama.

**Demo Program**

![Program ketika dijalankan](images/2-1.jpg)<br>
*File `.c` ketika dijalankan*

![Hasil extract drakor.zip](images/2-2.jpg)<br>
*Hasil extract drakor.zip*

![Isi folder genre](images/2-3.jpg)<br>
*Isi dari folder setiap genre*

![Isi file .txt](images/2-4.jpg)<br>
*Isi file `.txt`*

**Kendala Pengerjaan**

Pada pengerjaan soal 2 ini, kendala yang saya alami adalah pemisahan 2 drama yang tergabung dalam 1 file dan sort secara ascending. Untuk pemisahan 2 drama sudah berhasil diatasi, namun untuk ascending sampai saat ini masih belum.

## Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

Contoh : conan_rwx_hewan.png

Catatan :

Tidak boleh memakai system().

Tidak boleh memakai function C mkdir() ataupun rename().

Gunakan exec dan fork

Direktori “.” dan “..” tidak termasuk

**Penyelesaian Soal**

membuat proses baru menggunakan fungsi `fork` sehingga terdapat parent process dan child process
```
pid_t child_id;
	int status;

  child_id = fork();
```
kemudian mengecek apakah berhasil membuat proses baru :
```
if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
```
child process berisi perintah untuk membuat folder "modul2" dan di dalamnya terdapat folder "darat"
```
if (child_id == 0) {
    // child1 bikin folder modul2 & darat
	char *argv[] = {"mkdir", "-p", "/home/hasna/modul2/darat", NULL};
   	 execv("/bin/mkdir", argv);
  }
```
pada parent process menggunakan fungsi `wait` untuk menunggu child process selesai melakukan tugasnya
```
else {
  //parent1
  	while ((wait(&status)) >0);
  	sleep(3);
```
pada parent process tersebut, `sleep(3)` berfungsi untuk menunggu 3 detik terlebih dahulu sebelum membuat folder "air"

kemudian membuat process baru lagi menggunakan `fork` dimana child process selanjutnya berisi perintah untuk unzip file `animal.zip`
```
if (child_id == 0) {
    // child3 unzip file animal.zip
	char *argv[] = {"unzip", "animal.zip", "-d", "/home/hasna/modul2", NULL};
        execv("/bin/unzip", argv);
  	}
```
untuk parent process sama seperti sebelumnya, menggunakan fungsi `wait` untuk menunggu child process selesai melakukan tugasnya

kemudian membuat process baru lagi dimana child process nya berisi perintah untuk memisahkan file ke dalam folder berdasarkan kategori darat/air, dengan cara melakukan directory listing menggunakan library `dirent` yang telah dicontohkan di dalam modul 2 sisop
```
if (child_id == 0) {
    // child4 memisahkan file ke dalam folder berdasarkan kategori darat/air. 
    // file tanpa keterangan dihapus
	DIR *dp;
	    struct dirent *ep;
	    char string[256];
    	char string2[30];

	    dp = opendir("/home/hasna/modul2/animal");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      pid_t child_id;
	      int status;
	      child_id = fork();
	      if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }

	  if (child_id == 0){
	  //child 4a
	  strcpy(string, ep->d_name);
	  strcpy(string2, "/home/hasna/modul2/animal/");
	      if(strstr(string,"darat") != NULL){
	      strcat(string2, string);
			      char *argv[] = {"mv", string2, "/home/hasna/modul2/darat", NULL};
				execv("/bin/mv", argv);
		}
		else if(strstr(string,"air") != NULL){
	      strcat(string2, string);
			      char *argv[] = {"mv", string2, "/home/hasna/modul2/air", NULL};
				execv("/bin/mv", argv);
		}
		else{
	      strcat(string2, string);
			      char *argv[] = {"rm", string2, NULL};
				execv("/bin/rm", argv);
		}
	  }
	  else{
	  //parent 4a
	      while ((wait(&status)) > 0);
	      continue;
		}
	      }

	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
  	}
```
untuk parent dari process ini sama seperti sebelumnya, menggunakan fungsi `wait` untuk menunggu child process selesai melakukan tugasnya

kemudian membuat process baru lagi dimana child process berisi perintah untuk menghapus file "burung" dengan melakukan directory listing sama seperti sebelumnya
```
if (child_id == 0){
	  //child 5 menghapus file burung
	  	DIR *dp;
	    struct dirent *ep;
	    char string[256];
	    char string2[30];


	    dp = opendir("/home/hasna/modul2/darat");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      pid_t child_id;
	      int status;
	      child_id = fork();
	      if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }

	  if (child_id == 0) {
	  //child 5a
	  	strcpy(string, ep->d_name);
	  	strcpy(string2, "/home/hasna/modul2/darat/");
	      if(strstr(ep->d_name,"bird") != NULL){
		      strcat(string2, string);
		      char *argv[] = {"rm", string2, NULL};
		      execv("/bin/rm", argv);
		      }
		else break;
	  }
	  else{
	  //parent 5a
	      while ((wait(&status)) > 0);
	      continue;
		}	      
	      }

	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
		
	  }
```
untuk parent dari process ini sama seperti sebelumnya, menggunakan fungsi `wait` untuk menunggu child process selesai melakukan tugasnya

kemudian membuat process baru lagi dimana child process berisi perintah untuk membuat file `list.txt` di dalam folder air
```
 if (child_id == 0) {
	    // child 6 membuat file list.txt di folder air
		char *argv[] = {"touch", "/home/hasna/modul2/air/list.txt", NULL};
	   	 execv("/bin/touch", argv);
	  }
```
untuk parent dari process ini sama seperti sebelumnya, menggunakan fungsi `wait` untuk menunggu child process selesai melakukan tugasnya

kemudian membuat list nama semua hewan yang ada di dalam folder "air" ke `list.txt` dengan format UID_[UID file permission]_Nama File.[jpg/png] dengan cara melakukan directory listing untuk membaca nama-nama file di dalam folder "air"

menggunakan library `sys/stat.h`, `pwd.h`, dan `grp.h` untuk melihat owner dan permission dari suatu file dengan sintaks yang telah dicontohkan dalam modul 2 sisop
```
 else {
	  //parent6 menulis file list.txt
	  	while ((wait(&status)) > 0);
	  	DIR *dp;
	    struct dirent *ep;
	    char string[256];
	    char string2[30];
	    char uid[236];
	    
	    dp = opendir("/home/hasna/modul2/air");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      
	    strcpy(string, ep->d_name);
	    strcpy(string2, "/home/hasna/modul2/air/");
	    strcat(string2, string);
	    if((strcmp(string,".")==0)||(strcmp(string,"..")==0)||(strstr(string,".txt") != NULL)) continue;
	    struct stat info;
	    
	    int r;
	    r = stat(string2, &info);
	    if( r==-1 )
	    {
		fprintf(stderr,"File error\n");
		exit(1);
	    }

	    struct passwd *pw = getpwuid(info.st_uid);
	    strcpy(uid, pw->pw_name);
	    char fuid[236]={""};
	    //	Owner permissions
	    if( info.st_mode & S_IRUSR )
		strcat(fuid,"r");
	    if( info.st_mode & S_IWUSR )
		strcat(fuid,"w");
	    if( info.st_mode & S_IXUSR )
		strcat(fuid,"x");
	    char nametxt[100]={""};
	    strcat(nametxt, uid);
	    strcat(nametxt, "_");
	    strcat(nametxt, fuid);
	    strcat(nametxt, "_");
	    strcat(nametxt, string);
	    FILE *fp = fopen("/home/hasna/modul2/air/list.txt", "a");
	    if (fp == NULL)
	    {
		printf("Error opening the file");
		return -1;
	    }
	    // write to the text file
		fprintf(fp, "%s\n", nametxt);

	    // close the file
	    fclose(fp);
	      }
	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
	  }
```
**Demo Program**

![folder modul2](images/3-1.png) <br>
*folder modul2*

![folder darat](images/3-2.png) <br>
*folder darat*

![folder air](images/3-3.png) <br>
*folder air*

![file list.txt](images/3-4.png) <br>
*file list.txt*

**Kendala Pengerjaan**

(belum ada kendala)

