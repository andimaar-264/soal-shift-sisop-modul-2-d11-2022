#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

void unzip(char *zipdir, char *locdir)
{
  char *argv[] = {"unzip", zipdir, "*.png", "-d", locdir, NULL};
  execv("/bin/unzip", argv);
}

void mkDir(char *locdir)
{
  char *argv[] = {"mkdir", "-p", locdir, NULL};
  execv("/bin/mkdir", argv);
}

void copyDir(char *locdir, char *locdir1)
{
  char *argv[] = {"cp", locdir, locdir1, NULL};
  execv("/bin/cp", argv);
}

void moveDir(char *locdir, char *locdir1)
{
  char *argv[] = {"mv", locdir, locdir1, NULL};
  execv("/bin/mv", argv);
}

void rmDir(char *locdir)
{
  char *argv[] = {"rm", "-rf", locdir, NULL};
  execv("/bin/rm", argv);
}

void writeTxt(char *locdir, char *movieTitle, char *genre, char *year)
{
  FILE *fp;
  if (access(locdir, F_OK) == -1)
  {
    fp = fopen(locdir, "w");
    fprintf(fp, "kategori : %s\n", genre);
    fclose(fp);
  }

  fp = fopen(locdir, "a");
  fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", movieTitle, year);
  fclose(fp);
}

char *rType(char *s)
{
  int n, i;
  char *str;
  n = strlen(s) - 3;
  if (n < 1)
    return NULL;

  str = (char *)malloc(n);

  for (i = 0; i < n - 1; i++)
    str[i] = s[i];

  str[i] = '\0';
  return str;
}

int main()
{
  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    unzip("./drakor.zip", "/home/wicaksono/shift2/drakor");
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
      DIR *dp;
      char path[101] = "/home/wicaksono/shift2/drakor";
      dp = opendir(path);
      if (dp != NULL)
      {
        struct dirent *dir;
        while ((dir = readdir(dp)) != NULL)
        {
          if (dir->d_type == DT_REG)
          {
            char *eachMovie, *movie, *temp1, *temp2, copy1[101], copy2[101], copy3[101], base2[101], base3[101];
            char *reName = rType(dir->d_name);

            for (eachMovie = strtok_r(reName, "_", &temp1); eachMovie != NULL; eachMovie = strtok_r(NULL, "_", &temp1))
            {
              char title[101], year[101], genre[101];
              int i = 0;
              char base[101];
              strcpy(base, path);
              strcat(base, "/");

              strcpy(copy1, dir->d_name);
              strcpy(base2, base);
              strcpy(base3, base);
              strcpy(copy2, dir->d_name);
              strcpy(copy3, dir->d_name);

              for (movie = strtok_r(eachMovie, ";", &temp2); movie != NULL; movie = strtok_r(NULL, ";", &temp2))
              {
                if (i == 0)
                  strcpy(title, movie);
                else if (i == 1)
                  strcpy(year, movie);
                else
                  strcpy(genre, movie);
                i++;
              }

              strcat(base, genre);
              pid_t child_id;
              int status;
              child_id = fork();

              if (child_id == 0)
              {
                mkDir(base);
              }
              else
              {
                while ((wait(&status)) > 0)
                  ;
                char dataPath[100];
                stpcpy(dataPath, base);

                char movieTitle[100];
                strcpy(movieTitle, title);

                strcat(title, ".png");
                strcat(base2, copy2);

                pid_t child_id;
                int status;
                child_id = fork();

                if (child_id == 0)
                {
                  copyDir(base2, base);
                }
                else
                {
                  while ((wait(&status)) > 0)
                    ;
                  strcpy(base3, base);
                  strcat(base3, "/");
                  strcat(base3, copy2);
                  strcat(base, "/");
                  strcat(base, title);

                  pid_t child_id;
                  int status;
                  child_id = fork();

                  if (child_id == 0)
                  {
                    moveDir(base3, base);
                  }
                  else
                  {
                    while ((wait(&status)) > 0)
                      ;
                    strcat(dataPath, "/data.txt");
                    writeTxt(dataPath, movieTitle, genre, year);
                  }
                }
              }
            }
          }

          if (dir->d_type == DT_REG)
          {
            char base[101] = "/home/wicaksono/shift2/drakor/";
            strcat(base, dir->d_name);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id == 0)
            {
              rmDir(base);
            }
            else
            {
              while ((wait(&status)) > 0)
                ;
            }
          }
        }
      }
      closedir(dp);
    }
  }
  return 0;
}
