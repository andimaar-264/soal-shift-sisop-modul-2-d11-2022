#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

int main(){

  pid_t child_id;
	int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // child1 bikin folder modul2 & darat
	char *argv[] = {"mkdir", "-p", "/home/hasna/modul2/darat", NULL};
   	 execv("/bin/mkdir", argv);
  }
  else {
  //parent1
  	while ((wait(&status)) >0);
  	sleep(3);
	int status;

  	child_id = fork();
	if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if (child_id == 0) {
    // child2 bikin folder air
	char *argv[] = {"mkdir", "-p", "/home/hasna/modul2/air", NULL};
   	 execv("/bin/mkdir", argv);
  }
  else {
  //parent2
  	while ((wait(&status)) > 0);
	int status;
	  child_id = fork();
	  if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }
	  if (child_id == 0) {
    // child3 unzip file animal.zip
	char *argv[] = {"unzip", "animal.zip", "-d", "/home/hasna/modul2", NULL};
        execv("/bin/unzip", argv);
  	}
  	else{
  	//parent3
  		while ((wait(&status)) > 0);
  		int status;
  		child_id = fork();
  		if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }
	  if (child_id == 0) {
    // child4 memisahkan file ke dalam folder berdasarkan kategori darat/air. 
    // file tanpa keterangan dihapus
	DIR *dp;
	    struct dirent *ep;
	    char string[256];
    	char string2[30];

	    dp = opendir("/home/hasna/modul2/animal");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      pid_t child_id;
	      int status;
	      child_id = fork();
	      if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }

	  if (child_id == 0){
	  //child 4a
	  strcpy(string, ep->d_name);
	  strcpy(string2, "/home/hasna/modul2/animal/");
	      if(strstr(string,"darat") != NULL){
	      strcat(string2, string);
			      char *argv[] = {"mv", string2, "/home/hasna/modul2/darat", NULL};
				execv("/bin/mv", argv);
		}
		else if(strstr(string,"air") != NULL){
	      strcat(string2, string);
			      char *argv[] = {"mv", string2, "/home/hasna/modul2/air", NULL};
				execv("/bin/mv", argv);
		}
		else{
	      strcat(string2, string);
			      char *argv[] = {"rm", string2, NULL};
				execv("/bin/rm", argv);
		}
	  }
	  else{
	  //parent 4a
	      while ((wait(&status)) > 0);
	      continue;
		}
	      }

	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
  	}
  	else {
  	//parent4
  	while ((wait(&status)) > 0);
  	pid_t child_id;
	      int status;
	      child_id = fork();
	      if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }
	  if (child_id == 0){
	  //child 5 menghapus file burung
	  	DIR *dp;
	    struct dirent *ep;
	    char string[256];
	    char string2[30];


	    dp = opendir("/home/hasna/modul2/darat");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      pid_t child_id;
	      int status;
	      child_id = fork();
	      if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }

	  if (child_id == 0) {
	  //child 5a
	  	strcpy(string, ep->d_name);
	  	strcpy(string2, "/home/hasna/modul2/darat/");
	      if(strstr(ep->d_name,"bird") != NULL){
		      strcat(string2, string);
		      char *argv[] = {"rm", string2, NULL};
		      execv("/bin/rm", argv);
		      }
		else break;
	  }
	  else{
	  //parent 5a
	      while ((wait(&status)) > 0);
	      continue;
		}	      
	      }

	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
		
	  }
  	else {
  		while ((wait(&status)) > 0);
  	//parent5
  	pid_t child_id;
	int status;

	  child_id = fork();

	  if (child_id < 0) {
	    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	  }

	  if (child_id == 0) {
	    // child 6 membuat file list.txt di folder air
		char *argv[] = {"touch", "/home/hasna/modul2/air/list.txt", NULL};
	   	 execv("/bin/touch", argv);
	  }
	  else {
	  //parent6 menulis file list.txt
	  	while ((wait(&status)) > 0);
	  	DIR *dp;
	    struct dirent *ep;
	    char string[256];
	    char string2[30];
	    char uid[236];
	    
	    dp = opendir("/home/hasna/modul2/air");

	    if (dp != NULL)
	    {
	      while ((ep = readdir (dp))) {
	      
	    strcpy(string, ep->d_name);
	    strcpy(string2, "/home/hasna/modul2/air/");
	    strcat(string2, string);
	    if((strcmp(string,".")==0)||(strcmp(string,"..")==0)||(strstr(string,".txt") != NULL)) continue;
	    struct stat info;
	    
	    int r;
	    r = stat(string2, &info);
	    if( r==-1 )
	    {
		fprintf(stderr,"File error\n");
		exit(1);
	    }

	    struct passwd *pw = getpwuid(info.st_uid);
	    strcpy(uid, pw->pw_name);
	    char fuid[236]={""};
	    //	Owner permissions
	    if( info.st_mode & S_IRUSR )
		strcat(fuid,"r");
	    if( info.st_mode & S_IWUSR )
		strcat(fuid,"w");
	    if( info.st_mode & S_IXUSR )
		strcat(fuid,"x");
	    char nametxt[100]={""};
	    strcat(nametxt, uid);
	    strcat(nametxt, "_");
	    strcat(nametxt, fuid);
	    strcat(nametxt, "_");
	    strcat(nametxt, string);
	    FILE *fp = fopen("/home/hasna/modul2/air/list.txt", "a");
	    if (fp == NULL)
	    {
		printf("Error opening the file");
		return -1;
	    }
	    // write to the text file
		fprintf(fp, "%s\n", nametxt);

	    // close the file
	    fclose(fp);
	      }
	      (void) closedir (dp);
	    } else perror ("Couldn't open the directory");
	  }
  	}
  	}	
  	}
  }	
  }
}
